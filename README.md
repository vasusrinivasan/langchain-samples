# langchain-samples



## Getting started
1. This is a good starting point https://www.mlq.ai/getting-started-with-langchain/ 
2. Proceed to this tutorial https://www.mlq.ai/prompt-engineering/
3. https://wandb.ai/a-sh0ts/langchain_callback_demo/reports/Prompt-Engineering-LLMs-with-LangChain-and-W-B--VmlldzozNjk1NTUw
4. https://www.youtube.com/watch?v=f9_BWhCI4Zo  
