# To run do the following in terminal: export OPENAI_API_KEY="your-token"
# python3 a-first-prompt.py "What is the capital of USA?"

import argparse
import os
from langchain import OpenAI

os.environ['OPENAI_API_TOKEN'] = 'OPENAI_API_KEY'
llm = OpenAI()

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("question", help="Enter your question")
    args = parser.parse_args()

    print(llm(args.question))

if __name__ == '__main__':
    main()
