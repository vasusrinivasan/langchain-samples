# To run do the following in terminal: export OPENAI_API_KEY="your-token"
# python3 b-prompt-with-temperature.py "Suggest 5 names for a board games coffee store"

import argparse
import os
from langchain import OpenAI

os.environ['OPENAI_API_TOKEN'] = 'OPENAI_API_KEY'
llm = OpenAI(temperature=0.7)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("question", help="Enter your question")
    args = parser.parse_args()

    print(llm(args.question))

if __name__ == '__main__':
    main()
