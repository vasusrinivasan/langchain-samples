# To run do the following in terminal: export OPENAI_API_KEY="your-token"
# python3 c-prompt-template-with-chains.py "Suggest 5 names for a board games coffee store"

import argparse
import os
from langchain import OpenAI, LLMChain, PromptTemplate

os.environ['OPENAI_API_TOKEN'] = 'OPENAI_API_KEY'

davinci = OpenAI()

template = """
I want you to act as a naming consultant for new companies.
What is a good name for a company that makes {product}?
"""

prompt = PromptTemplate(
    input_variables=["product"],
    template=template
)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("question", help="Enter your question")
    args = parser.parse_args()

    # chain Prompt with query template
    llm_chain = LLMChain(prompt=prompt,llm=davinci)

    print(llm_chain.run(args.question))


if __name__ == '__main__':
    main()
