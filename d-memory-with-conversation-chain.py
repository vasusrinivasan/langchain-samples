# To run do the following in terminal: export OPENAI_API_KEY="your-token"
# python3 d-memory-with-conversation-chain.py 

import os
from langchain import OpenAI,ConversationChain

llm = OpenAI()


os.environ['OPENAI_API_TOKEN'] = 'OPENAI_API_KEY'

def main():
    
    llm = OpenAI(temperature=0)
    conversation = ConversationChain(llm=llm)

    # Start the conversation by greeting the user
    print("Conversational Bot: Hi there!")
    while True:
        # Get user input from the terminal
        user_input = input("You: ")
        # Check if the user wants to exit
        if user_input.lower() == "bye":
            print("Bot: Goodbye!")
            break
        # Predict the response from the AI model
        response = conversation.predict(input=user_input)
        # Print the response from the model
        print("Conversational Bot: " + response)



if __name__ == '__main__':
    main()
