# To run do the following in terminal: export OPENAI_API_KEY="your-token"
# python3 e-summarize.py ./doc.pdf 

import argparse
import os
from langchain import OpenAI
from langchain.chains.summarize import load_summarize_chain
from langchain.document_loaders import PyPDFLoader


os.environ['OPENAI_API_TOKEN'] = 'OPENAI_API_KEY'
llm = OpenAI()

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("pdf", help="Attach your file")
    args = parser.parse_args()

    loader = PyPDFLoader(args.pdf)
    docs = loader.load_and_split()
    chain = load_summarize_chain(llm,chain_type="refine")
    summary = chain.run(docs)
    
    print(summary)

if __name__ == '__main__':
    main()


