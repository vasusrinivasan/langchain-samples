# To run do the following in terminal: export OPENAI_API_KEY="your-token"
# python3 f-llm-chain.py ./doc.pdf 

import argparse
import os
from langchain import OpenAI
from langchain.document_loaders import PyPDFLoader
from langchain.prompts import PromptTemplate
from langchain.chains import LLMChain

prompt = PromptTemplate(
    input_variables=["resume"],
    template="Generate a hypothetical job description for this {resume} with job title, responsibilities and qualifications and no company name and no location",
)


os.environ['OPENAI_API_TOKEN'] = 'OPENAI_API_KEY'
llm = OpenAI()

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("pdf", help="Attach your file")
    args = parser.parse_args()

    loader = PyPDFLoader(args.pdf)
    resume = loader.load()
    chain = LLMChain(llm=llm, prompt=prompt)
    print(chain.run(resume))

if __name__ == '__main__':
    main()



