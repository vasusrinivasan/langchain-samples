# To run do the following in terminal: export OPENAI_API_KEY="your-token"
# python3 f-job-desc.py --resume ./resume.pdf --job ./req.txt

import argparse
import os
from langchain import OpenAI
from langchain.document_loaders import PyPDFLoader
from langchain.prompts import PromptTemplate
from langchain.chains import LLMChain

prompt = PromptTemplate(
    input_variables=["resume","job"],
    template="Find the fitness of this {job} for this {resume} starting with one word, by classifying into one of perfect, high, medium, low followed by a statement justifying your claim. Give the response in a json format with fitness and text",
)

os.environ['OPENAI_API_TOKEN'] = 'OPENAI_API_KEY'
llm = OpenAI()

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument("--resume")
    parser.add_argument("--job")

    args = parser.parse_args()

    loader = PyPDFLoader(args.resume)
    with open(args.job, 'r') as file:
        job = file.read().replace('\n', '')

    resume = loader.load()
    chain = LLMChain(llm=llm, prompt=prompt)
    result = chain(inputs={"resume":resume, "job":job})

    print(result['text'])
    #print(result.metadata.text)

if __name__ == '__main__':
    main()



